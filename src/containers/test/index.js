import React from 'react';
import {connect} from 'react-redux';
import * as actions from '../../store/actions';

class Test extends React.Component {

  state = {
      edition: false,
    };


  redactItem = () => {
    this.setState({edition: true});
  }

  stopEdition = (id, e) => {
    e.stopPropagation();
    this.setState({edition: false});
    let newValue = e.target.value;
    let editEl = {};
    this.props.items.map(item => {
      if (item._id == id) {
        editEl = item;
        this.props.dispatch(actions.list.changeTitle(editEl, newValue));
      }
    });
  }

  buildTree = (list, parentId) => {
    if (Array.isArray(list[parentId])) {
      return list[parentId].map(listItem => {
        return (
          <ul className='list__body' key={listItem._id}>
            <li className='list__item' onClick={this.redactItem}>
              {!this.state.edition
                ? <span className='list__item-title'>{listItem.title}</span>
                : <input
                  className='list__item-input'
                  type="text"
                  defaultValue={listItem.title}
                  onBlur={(event) => this.stopEdition(listItem._id, event)}
                  onKeyUp={(event) => {
                    if(event.keyCode == 13) {
                      this.stopEdition(listItem._id, event);
                    }}}
                />
              }

              {this.buildTree(list, listItem._id)}
            </li>
          </ul>
        );
      });
    }
  }

  componentDidMount(){
    this.props.dispatch(actions.list.getData());
  }

  render() {
    return (
      <div className='test'>
        <h2 className='test__title'>Тестовое</h2>
        <div className='test__wrapper'>
          {this.buildTree(this.props.tree, 0)}
        </div>
      </div>
    );
  }
}

export default connect(state => ({
    items: state.list.items,
    tree: state.list.tree,
  }))(Test);